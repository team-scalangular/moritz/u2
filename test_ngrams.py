from data import make_ngram_vectors, make_ngram_dictionary
import torch

n_gram_tests = [
    {
        "n_gram": 1,
        "unk_threshold": 0,
        "expected_dictionary": {
            "UNK": 0,
            "A": 1,
            "really": 2,
            "great": 3,
            "movie": 4,
            "I": 5,
            "didn't": 6,
            "like": 7,
            "the": 8,
            "at": 9,
            "all": 10,
            "What": 11,
            "a": 12,
            "that": 13,
            "was": 14,
            "The": 15,
            "so": 16,
            "boring": 17,
            "fell": 18,
            "asleep": 19,
        },
        "expected_vectors": [
            torch.tensor([[1, 2, 3, 4]]),
            torch.tensor([[5, 6, 7, 8, 4, 9, 10]]),
            torch.tensor([[11, 12, 3, 4, 13, 14]]),
            torch.tensor([[15, 4, 14, 16, 17, 13, 5, 18, 19]]),
        ],
    },
    {
        "n_gram": 1,
        "unk_threshold": 1,
        "expected_dictionary": {
            "UNK": 0,
            "great": 1,
            "movie": 2,
            "I": 3,
            "that": 4,
            "was": 5,
        },
        "expected_vectors": [
            torch.tensor([[0, 0, 1, 2]]),
            torch.tensor([[3, 0, 0, 0, 2, 0, 0]]),
            torch.tensor([[0, 0, 1, 2, 4, 5]]),
            torch.tensor([[0, 2, 5, 0, 0, 4, 3, 0, 0]]),
        ],
    },
    {
        "n_gram": 2,
        "unk_threshold": 0,
        "expected_dictionary": {
            "UNK": 0,
            "A": 1,
            "really": 2,
            "great": 3,
            "movie": 4,
            "A really": 5,
            "really great": 6,
            "great movie": 7,
            "I": 8,
            "didn't": 9,
            "like": 10,
            "the": 11,
            "at": 12,
            "all": 13,
            "I didn't": 14,
            "didn't like": 15,
            "like the": 16,
            "the movie": 17,
            "movie at": 18,
            "at all": 19,
            "What": 20,
            "a": 21,
            "that": 22,
            "was": 23,
            "What a": 24,
            "a great": 25,
            "movie that": 26,
            "that was": 27,
            "The": 28,
            "so": 29,
            "boring": 30,
            "fell": 31,
            "asleep": 32,
            "The movie": 33,
            "movie was": 34,
            "was so": 35,
            "so boring": 36,
            "boring that": 37,
            "that I": 38,
            "I fell": 39,
            "fell asleep": 40,
        },
        "expected_vectors": [
            torch.tensor([[1, 2, 3, 4, 5, 6, 7]]),
            torch.tensor([[8, 9, 10, 11, 4, 12, 13, 14, 15, 16, 17, 18, 19]]),
            torch.tensor([[20, 21, 3, 4, 22, 23, 24, 25, 7, 26, 27]]),
            torch.tensor(
                [[28, 4, 23, 29, 30, 22, 8, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]]
            ),
        ],
    },
    {
        "n_gram": 2,
        "unk_threshold": 1,
        "expected_dictionary": {
            "UNK": 0,
            "great": 1,
            "movie": 2,
            "great movie": 3,
            "I": 4,
            "that": 5,
            "was": 6,
        },
        "expected_vectors": [
            torch.tensor([[0, 0, 1, 2, 0, 0, 3]]),
            torch.tensor([[4, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0]]),
            torch.tensor([[0, 0, 1, 2, 5, 6, 0, 0, 3, 0, 0]]),
            torch.tensor([[0, 2, 6, 0, 0, 5, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]),
        ],
    },
]

if __name__ == "__main__":
    for test in n_gram_tests:
        print(test)
        data = [
            (
                "A really great movie".split(),
                "positive",
            ),
            ("I didn't like the movie at all".split(), "negative"),
            ("What a great movie that was".split(), "positive"),
            ("The movie was so boring that I fell asleep".split(), "negative"),
        ]
        ngram_dictionary = make_ngram_dictionary(
            data, max_ngrams=test["n_gram"], unk_threshold=test["unk_threshold"]
        )

        assert (
            ngram_dictionary == test["expected_dictionary"]
        ), f"Expected\n{test['expected_dictionary']}\nbut got\n{ngram_dictionary}"

        vectors = []
        for sentence in data:
            vectors.append(
                make_ngram_vectors(
                    sentence[0], ngram_dictionary, max_ngrams=test["n_gram"]
                )
            )

        for i, vector in enumerate(vectors):
            assert torch.all(
                vector == test["expected_vectors"][i]
            ), f"Expected {test['expected_vectors'][i]} but got {vector}"

    print("All tests passed!")
