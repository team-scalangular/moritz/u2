from typing import Dict

from scipy.spatial import distance
from gensim.models.keyedvectors import Word2VecKeyedVectors

from data import load_odd_one_out


def main():
    # Load the pretrained embeddings
    # pretrained_word_embeddings_file = "pretrained_embeddings/glove-wiki-gigaword-50.txt"
    pretrained_word_embeddings_file = "pretrained_embeddings/fasttext300d.vec"
    data = load_odd_one_out()
    pretrained_embeddings = Word2VecKeyedVectors.load_word2vec_format(
        pretrained_word_embeddings_file
    )

    distance_function_to_use = distance.cosine  # use one of these: distance.canberra distance.euclidean

    for example in data:
        find_odd_one_out(distance_function_to_use, example, pretrained_embeddings)


def find_odd_one_out(distance_function_to_use, example, pretrained_embeddings):
    print(f"figuring out odd one in {example}")
    similarity_by_pairing: Dict[tuple[str, str], float] = {}
    calculate_distance_for_all_pairings(distance_function_to_use, example, pretrained_embeddings,
                                        similarity_by_pairing)

    similarity_over_all_by_word: Dict[str, float] = {}
    calculate_distance_for_each_word(similarity_by_pairing, similarity_over_all_by_word)

    if len(similarity_over_all_by_word) == 0:
        print(f"cannot compare {example}")
    else:
        odd_one_out = max(similarity_over_all_by_word, key=similarity_over_all_by_word.get)
        print(f"Odd one out in {example} is {odd_one_out}.")
        print(10 * "-")


def calculate_distance_for_each_word(similarity_by_pairing: Dict[tuple[str, str], float],
                                     similarity_over_all_by_word: Dict[str, float]):
    for i, pairing in enumerate(similarity_by_pairing):
        (wordA, wordB) = pairing
        add_to_dict(pairing, similarity_by_pairing, similarity_over_all_by_word, wordA)
        add_to_dict(pairing, similarity_by_pairing, similarity_over_all_by_word, wordB)


def calculate_distance_for_all_pairings(distance_function_to_use, example, pretrained_embeddings,
                                        similarity_by_pairing: Dict[tuple[str, str], float]):
    for word1 in example:
        for word2 in example:
            if word1 != word2:
                if word1 in pretrained_embeddings and word2 in pretrained_embeddings:
                    # way 1 (using .similarity of glove)
                    # use min in decision
                    # similarity_by_pairing[(word1, word2)] = pretrained_embeddings.similarity(word1, word2)

                    # way 2
                    # use max in decision
                    word1vec = pretrained_embeddings[pretrained_embeddings.get_index(word1)]
                    word2vec = pretrained_embeddings[pretrained_embeddings.get_index(word2)]
                    similarity_by_pairing[(word1, word2)] = distance_function_to_use(word1vec.transpose(),
                                                                                     word2vec.transpose())


def add_to_dict(pairing, similarity_by_pairing, similarity_over_all_by_word, word):
    if word in similarity_over_all_by_word:
        similarity_over_all_by_word[word] += similarity_by_pairing[pairing]
    else:
        similarity_over_all_by_word[word] = similarity_by_pairing[pairing]


if __name__ == "__main__":
    main()
